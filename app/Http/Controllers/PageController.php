<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

class PageController extends Controller
{
    public function index()
    {
        $keyword = '';
        // show post, status = 1, order by created at desc
        $posts = Post::where('status', '=', 1)->orderBy('created_at', 'DESC')->paginate(5);

        return view('pages.index', compact('posts', 'keyword'));
    }

    public function show(Post $post)
    {
        return view('pages.show', compact('post'));
    }

    public function search()
    {
        $keyword = request()->get('keyword');

        $posts = Post::where('status','=',1)->where('title', 'LIKE', '%' . $keyword . '%')->orWhere('content', 'LIKE', '%' . $keyword . '%')->orderBy('created_at', 'DESC')->paginate(5);

        return view('pages.index', compact('posts', 'keyword'));
    }
}
