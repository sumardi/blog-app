@extends('layouts.main')

@section('content')
    <ol class="breadcrumb">
        <li><a href="{{ url('/') }}">Home</a></li>
        <li class="active">Post</li>
    </ol>

    <h3>{{ title_case($post->title) }}</h3>
    <p>{{ $post->content }}</p>

    <p>
        By {{ $post->user->name }} |
        Category: {{ $post->categories->implode('name', ',') }} |
        {{ $post->updated_at->diffForHumans() }}
    </p>

    <h3 class="page-header">
        Comments
        <span class="badge badge-default">{{ $post->comments->count() }}</span>
    </h3>

    @forelse($post->comments as $comment)
        <div>
            <h4>{{ $comment->subject }}</h4>
            <p>{{ $comment->message }}</p>
            <p class="description">
                <small>
                    Posted By : {{ $comment->user->name }} |
                    {{ $comment->created_at->diffForHumans() }}
                </small>
            </p>
        </div>
    @empty
        No Comment
    @endforelse

    @if(auth()->check())
        @if($errors->count() > 0)
            <div class="alert alert-danger">
                <strong>Validation errors:</strong> <br />
                <ul>
                @foreach($errors->all() as $error)
                     <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        @endif

    <form action="{{ route('comment.create', $post->id) }}" method="POST">
        {!! csrf_field() !!}
        <div class="form-group">
            <label for="">Subject</label>
            <input type="text" name="subject" class="form-control">
        </div>
        <div class="form-group">
            <label for="">Message</label>
            <textarea name="message" cols="30" rows="5" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Submit</button>
    </form>
    @endif
@stop