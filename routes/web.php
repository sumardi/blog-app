<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('page.index');
Route::get('/posts/{post}/show', 'PageController@show')->name('page.show');
Route::post('/posts/{post}/comment', 'CommentController@store')->name('comment.create')->middleware('auth');
Route::get('/search', 'PageController@search')->name('page.search');

Auth::routes();

Route::get('/admin', 'HomeController@index')->name('home')->middleware('can:create,App\Post');


// Route Post
//Route::get('/posts', 'PostController@index')->name('post.index');
//Route::get('/posts/submit', 'PostController@create')->name('post.create');
//Route::post('/posts/submit', 'PostController@store')->name('post.store');

// Post Group
// {id} - route parameter
// http://laravel.com/docs/5.4/routing
Route::group(['prefix' => 'posts', 'middleware' => 'auth'], function () {
    Route::get('/', 'PostController@index')->name('post.index')->middleware('can:create,App\Post');
    Route::get('/submit', 'PostController@create')->name('post.create')->middleware('can:create,App\Post');
    Route::post('/submit', 'PostController@store')->name('post.store')->middleware('can:create,App\Post');
    Route::get('/{post}/edit/', 'PostController@edit')->name('post.edit')->middleware('can:update,post');
    Route::get('/{post}/delete', 'PostController@delete')->name('post.delete')->middleware('can:delete,post');
    Route::post('/{post}/update', 'PostController@update')->name('post.update')->middleware('can:update,post');
});