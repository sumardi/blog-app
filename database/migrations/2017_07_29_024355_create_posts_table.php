<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // http://laravel.com/docs/5.4/migrations
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id'); // PK, auto_increment
            $table->string('title');
            $table->text('content')->nullable();
            $table->tinyInteger('status')->default(0)->unsigned();
            $table->integer('user_id')->unsigned()->default(0);
            $table->timestamps(); // created_at, updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
