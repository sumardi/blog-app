@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-header">Manage Post
                <div class="pull-right">
                    <a href="{{ route('post.create') }}" class="btn btn-primary">Create Post</a>
                </div>
                </h2>

                <div class="panel panel-default">
                    <div class="panel-heading">My Posts</div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>ID</th>
                                <th>Title</th>
                                <th>Categories</th>
                                <th>Posted Date</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            @foreach($posts as $p)
                                <tr>
                                    <td>{{ $p->id }}</td>
                                    <td>{{ $p->title }}</td>
                                    <td>
                                        {{ $p->categories->implode('name', ', ') }}
                                    </td>
                                    <td>{{ $p->created_at->format('d M Y h:i a') }}</td>
                                    <td>@include('posts.status_label', ['status' => $p->status])</td>
                                    <td>
                                        <a href="{{ route('post.edit', $p->id) }}" class="btn btn-primary btn-sm">Edit</a>
                                        <a href="{{ route('post.delete', $p->id) }}" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure?')">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop