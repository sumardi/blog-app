<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Comment;

class CommentController extends Controller
{
    public function store(Post $post)
    {
        // https://laravel.com/docs/5.4/validation
        // validate(request, rules, messages (optional))
        $this->validate(request(), [
            'subject' => 'required|min:3|max:50',
            'message' => 'min:3|max:200'
        ], [
            'subject.required' => 'The subject is required.'
        ]);

        // mass assignable
        $comment = new Comment(request()->only(['subject','message']));
        // optional: database field post_id - default = 0
        $comment->post_id = $post->id;
        $comment->user_id = auth()->user()->id;
        $comment->save();

        if ($post->comments()->save($comment)) {
            return redirect()->route('page.show', $post);
        }
    }
}
