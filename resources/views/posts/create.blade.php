@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-header">New Post</h2>
                <form action="{{ route('post.store') }}" method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Content</label>
                        <textarea name="content" id="" cols="30" rows="10" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Categories</label>
                        <select name="categories[]" class="form-control" multiple>
                            @foreach($categories as $id => $name)
                                <option value="{{ $id }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>
                        <select name="status" id="" class="form-control">
                            <option value="0" selected>Draft</option>
                            <option value="1">Published</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Save</button>
                </form>
            </div>
        </div>
    </div>
@stop