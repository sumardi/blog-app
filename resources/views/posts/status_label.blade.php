@if($status == 0)
    <span class="label label-default">Draft</span>
@elseif($status == 1)
    <span class="label label-success">Published</span>
@else
    <span class="label label-danger">n/a</span>
@endif