@extends('layouts.main')

@section('content')
    <p>
    @if(str_contains(request()->getRequestUri(), 'search'))
        <h3>Results for keyword "{{ $keyword }}"</h3>
    @endif
    </p>

    @forelse($posts as $p)
        <h3>
            <a href="{{ route('page.show', $p->id) }}">{{ $p->title }}</a>
        </h3>
        <p>{{ str_limit($p->content, 200) }}</p>
        <p class="description">
            By : {{ $p->user->name }} |
            {{ $p->updated_at->diffForHumans() }} |
            Category: {{ $p->categories->implode('name', ',') }} |
            Comments <span class="badge badge-default">{{ $p->comments->count() }}</span>
        </p>
        <hr>
    @empty
        No Post
    @endforelse

    {{ $posts->appends(['keyword' => $keyword])->links() }}
@stop