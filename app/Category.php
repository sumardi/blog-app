<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    // Category belongs to many post (many-to-many)
    public function posts()
    {
        return $this->belongsToMany('App\Post');
    }
}
