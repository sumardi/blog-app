@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="page-header">Edit Post</h2>
                <form action="{{ route('post.update', $post->id) }}" method="POST">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title" class="form-control" value="{{ $post->title }}">
                    </div>
                    <div class="form-group">
                        <label for="">Content</label>
                        <textarea name="content" id="" cols="30" rows="10" class="form-control">{{ $post->content }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Categories</label>
                        <select name="categories[]" class="form-control" multiple>
                            @foreach($categories as $id => $name)
                                <option value="{{ $id }}" @if($post->categories->contains($id)) selected @endif>{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Status</label>

                        <select name="status" id="" class="form-control">
                            <option value="0" @if($post->status == 0) selected @endif>Draft</option>
                            <option value="1" @if($post->status == 1) selected @endif>Published</option>

                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Save</button>
                </form>
            </div>
        </div>
    </div>
@stop