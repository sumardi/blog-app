<?php

namespace App\Policies;

use App\User;
use App\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // create post
    // user_level = 1 allowed, else denied
    public function create(User $user)
    {
        return $user->user_level == 1;
    }

    // update post
    // owner = allowed, else denied
    public function update(User $user, Post $post)
    {
        return $user->id == $post->user_id;
    }

    // delete post
    public function delete(User $user, Post $post)
    {
        return $user->id == $post->user_id;
    }
}
