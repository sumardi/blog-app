<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;

class PostController extends Controller
{
    // Retrieve
    public function index()
    {
        /*
         *   authentication -> user model (App\User.php)
         *   -> relations -> join table/model ([relations])
         *   -> fetch all
         *
         *   get() -> fetch all
         *   first() -> first result/row
         *   last() -> last result
         *    https://laravel.com/docs/5.4/eloquent
         */
        $posts = auth()->user()->posts()->with('categories')->get();

        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::orderBy('name','ASC')->get()->pluck('name', 'id');

        return view('posts.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $post = new Post();
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->status = (int) $request->get('status');
        $post->user_id = auth()->user()->id;

        if ($post->save()) {
            // many-to-many
            $post->categories()->sync($request->get('categories'));

//            return redirect('/posts');
            return redirect()->route('post.index');
        }
    }

    public function edit(Post $post)
    {
        // SELECT * FROM users WHERE id = $id
        // $post = Post::find($id)
//        $post = Post::findOrFail($id);

        $categories = Category::orderBy('name','ASC')->get()->pluck('name', 'id');

        return view('posts.edit', compact('post', 'categories'));
    }

    public function delete(Post $post)
    {
        if ($post->delete()) {
            return redirect()->route('post.index');
        }
    }

    public function update(Post $post, Request $request)
    {
        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->status = $request->get('status');

        if ($post->save()) {
            $post->categories()->sync($request->get('categories'));

            return redirect()->route('post.index');
        }
    }
}
