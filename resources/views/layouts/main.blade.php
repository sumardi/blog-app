<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <h2 class="page-header">
                    <a href="{{ url('/') }}">My Blog</a>
                </h2>
                @yield('content')
            </div>
            <div class="col-md-3">
                <h3 class="page-header">Search</h3>
                <form method="GET" action="{{ route('page.search') }}">
                    <div class="form-group">
                        <input type="text" class="form-control" name="keyword" placeholder="Search by title or content">
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>

                <h3 class="page-header">Menu</h3>
                <ul class="list-group">
                    @if(auth()->guest())
                        <!-- guest -->
                    <li class="list-group-item">
                        <a href="{{ url('/login') }}">Login</a>
                    </li>
                    <li class="list-group-item">
                        <a href="{{ url('/register') }}">Register</a>
                    </li>
                    @else
                        <!-- admin -->
                        @if(auth()->user()->isAdmin())
                        <li class="list-group-item">
                            <a href="{{ route('home') }}">Admin Panel</a>
                        </li>
                        @endif
                    @endif

                    @if(auth()->check())
                        <li class="list-group-item">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                        @endif
                </ul>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>