<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        $categories = array('Sports', 'News', 'Politics', 'Religion');

        foreach($categories as $category) {
            $c = new \App\Category();
            $c->name = $category;
            $c->save();
        }
    }
}
