<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // the attributes that can be mass assignable
    protected $fillable = [
        'subject', 'message'
    ];

    // Optional
    protected $table = 'comments';

    // Comment belongs to post
    public function post()
    {
        return $this->belongsTo('App\Post');
    }

    // Comment belongs to user
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
