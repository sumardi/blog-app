<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';

    // Post belongs to user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // Post has many comments (one-to-many)
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    // Post belongs to many categories (many-to-many)
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
